/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Negocio;

import DAO.CajeroJpaController;
import DAO.ClienteJpaController;
import DAO.Conexion;
import DAO.CuentaJpaController;
import DAO.MovimientoJpaController;
import DAO.TipoJpaController;
import DAO.TipoMovimientoJpaController;
import DTO.Cajero;
import DTO.Cliente;
import DTO.Cuenta;
import DTO.Movimiento;
import static DTO.Movimiento_.idTipoMovimiento;
import DTO.Tipo;
import DTO.TipoMovimiento;
import java.util.Date;

/**
 *
 * @author discarok
 */
public class Banco {

    private Conexion con;

    public Banco() {
        this.con = Conexion.getConexion();
    }

    public boolean insertarCliente(int cedula, String nombre, String fecha, String direccion, int tel, String email) {

        try {
            ClienteJpaController clienteDAO = new ClienteJpaController(con.getBd());
            Cliente cliente = new Cliente();
            cliente.setCedula(cedula);
            cliente.setNombre(nombre);
            cliente.setDircorrespondencia(direccion);
            cliente.setFechanacimiento(fechaNac(fecha));
            cliente.setTelefono(tel);
            cliente.setEmail(email);
            clienteDAO.create(cliente);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("No funciona");
            return false;
        }

    }

    public boolean loguear(String usuario, String contra) {
        try {
            Conexion con = Conexion.getConexion();
            CajeroJpaController cajeroDAO = new CajeroJpaController(con.getBd());
            Cajero nuevo = new Cajero();
            nuevo = cajeroDAO.findCajero(usuario);
            if (nuevo.getContrasena().equalsIgnoreCase(contra)) {
                return true;
            }
        } catch (Exception e) {
            System.out.println("error: " + e);
        }
        return false;
    }

    public Date fechaNac(String fecha) {
        String fechas[] = fecha.split("-");
        int agno = Integer.parseInt(fechas[0]) - 1900;
        int mes = Integer.parseInt(fechas[1]) - 1;
        int dia = Integer.parseInt(fechas[2]);
        Date d = new Date(agno, mes, dia);

        return d;
    }
    public String[] s(String arr) {
        String [] arra= new String[50];
        arra= arr.split("-");
        return arra;
    }
      public int s2(String arr[]) {
        String aux = null;
        for (int i = 0; i < 1; i++) {
            aux=arr[i];
            
        }
        return Integer.parseInt(aux);
    }
       public boolean editarCuenta(int nroCuenta, double saldo, String fecha, int tipo) {
        try {
            CuentaJpaController cuentaDAO = new CuentaJpaController(con.getBd());
            Cuenta cuenta = new Cuenta();
            cuenta.setNroCuenta(nroCuenta);
         cuenta.setFechacreacion(fechaNac(fecha));
         cuenta.setTipo(buscarTipo(tipo));
         cuenta.setSaldo(saldo);
            edCuent(nroCuenta, saldo, fecha,  tipo);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("No funciona");
            return false;
        }
    }

    public void edCuent(int nroCuenta, double saldo, String fecha, int tipo) {
        try {
            CuentaJpaController cuentaDAO = new CuentaJpaController(con.getBd());
            Cuenta cuenta = cuentaDAO.findCuenta(nroCuenta);
          cuenta.setFechacreacion(fechaNac(fecha));
          cuenta.setNroCuenta(nroCuenta);
          cuenta.setSaldo(saldo);
          cuenta.setTipo(buscarTipo(tipo));
            cuentaDAO.edit(cuenta);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public boolean insertarCuenta(int nroCuenta, double saldo, String fecha, int cedula, int tipo) {
        try {
            CuentaJpaController cuentaDAO = new CuentaJpaController(this.con.getBd());
            Cuenta cuenta = new Cuenta(nroCuenta, saldo, fechaNac(fecha));
            cuenta.setTipo(buscarTipo(tipo));
            cuenta.setCedula(buscarCliente(cedula));
            cuentaDAO.create(cuenta);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public Tipo buscarTipo(int i) {
        try {
            TipoJpaController tipoDAO = new TipoJpaController(this.con.getBd());
            Tipo tipo = tipoDAO.findTipo(i);
            return tipo;
        } catch (Exception e) {
            e.printStackTrace();
            System.err.println(e);
            return null;
        }
    }

    public Cliente buscarCliente(int cedula) {
        try {
            ClienteJpaController clienteDAO = new ClienteJpaController(con.getBd());
            Cliente c = clienteDAO.findCliente(cedula);
            return c;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public boolean consignar(int nroCuenta, String fecha, int saldo) {
        try {
            MovimientoJpaController movimientoDAO = new MovimientoJpaController(this.con.getBd());
            TipoMovimientoJpaController tipoMoDAO = new TipoMovimientoJpaController(this.con.getBd());
            CuentaJpaController cuentaDAO = new CuentaJpaController(this.con.getBd());
            Cuenta cuen = cuentaDAO.findCuenta(nroCuenta);
            TipoMovimiento idMov = tipoMoDAO.findTipoMovimiento(1);
            Movimiento mov = new Movimiento();
            mov.setFecha(fechaNac(fecha));
            mov.setValor(saldo);
            mov.setNroCuenta(cuen);
            mov.setIdTipoMovimiento(idMov);
            movimientoDAO.create(mov);
            seConsigno(nroCuenta, saldo);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            System.err.println(e);
        }
        return false;
    }

    //Edicion Cuenta
    public void seConsigno(int nroCuenta, int saldo) {
        try {
            CuentaJpaController cuentaDAO = new CuentaJpaController(this.con.getBd());
            Cuenta cuen = cuentaDAO.findCuenta(nroCuenta);
            cuen.setSaldo(cuen.getSaldo() + saldo);
            cuentaDAO.edit(cuen);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public boolean retirar(int nroCuenta, String fecha, int saldo) {
        try {
            MovimientoJpaController movimientoDAO = new MovimientoJpaController(this.con.getBd());
            TipoMovimientoJpaController tipoMoDAO = new TipoMovimientoJpaController(this.con.getBd());
            CuentaJpaController cuentaDAO = new CuentaJpaController(this.con.getBd());
            Cuenta cuen = cuentaDAO.findCuenta(nroCuenta);
            TipoMovimiento idMov = tipoMoDAO.findTipoMovimiento(2);
            Movimiento mov = new Movimiento();
            if (cuen.getSaldo() - saldo < 0) {
                return false;
            } else {
                mov.setFecha(fechaNac(fecha));
                mov.setValor(saldo);
                mov.setNroCuenta(cuen);
                mov.setIdTipoMovimiento(idMov);
                movimientoDAO.create(mov);
                seRetiro(nroCuenta, saldo);
                return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
            System.err.println(e);
        }
        return false;
    }

    public void seRetiro(int nroCuenta, int saldo) {
        try {
            CuentaJpaController cuentaDAO = new CuentaJpaController(this.con.getBd());
            Cuenta cuen = cuentaDAO.findCuenta(nroCuenta);
            cuen.setSaldo(cuen.getSaldo() - saldo);
            cuentaDAO.edit(cuen);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public boolean editarCliente(int cedula, String nombre, String fecha, String direccion, int tel, String email) {
        try {
            ClienteJpaController clienteDAO = new ClienteJpaController(con.getBd());
            Cliente cliente = new Cliente();
            cliente.setNombre(nombre);
            cliente.setDircorrespondencia(direccion);
            cliente.setFechanacimiento(fechaNac(fecha));
            cliente.setTelefono(tel);
            cliente.setEmail(email);
            eCli(cedula, email, fecha, direccion, tel, nombre);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("No funciona");
            return false;
        }
    }

    public void eCli(int cedula, String email, String fecha, String direccion, int tel, String nombre) {
        try {
            ClienteJpaController clienteDAO = new ClienteJpaController(con.getBd());
            Cliente clie = clienteDAO.findCliente(cedula);
            clie.setNombre(nombre);
            clie.setEmail(email);
            clie.setFechanacimiento(fechaNac(fecha));
            clie.setTelefono(tel);
            clie.setDircorrespondencia(direccion);
            clienteDAO.edit(clie);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public boolean eliminarCliente(int cedula) {
        try {
            ClienteJpaController clienteDAO = new ClienteJpaController(con.getBd());
            Cliente cliente = new Cliente();
            elimCli(cedula);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("No funciona");
            return false;
        }
    }

    public void elimCli(int cedula) {
        try {
            ClienteJpaController clienteDAO = new ClienteJpaController(con.getBd());
            Cliente clie = clienteDAO.findCliente(cedula);
            clienteDAO.destroy(cedula);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    
    public boolean eliminarCuenta(int cuenta) {
        try {
            CuentaJpaController cuentaDAO = new CuentaJpaController(con.getBd());
            Cuenta cue = new Cuenta();
            elimCuen(cuenta);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("No funciona");
            return false;
        }
    }

    public void elimCuen(int cuenta) {
        try {
       CuentaJpaController cuentaDAO = new CuentaJpaController(con.getBd());
            Cuenta cue = cuentaDAO.findCuenta(cuenta);
            cuentaDAO.destroy(cuenta);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    


}
