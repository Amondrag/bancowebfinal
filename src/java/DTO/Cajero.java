/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DTO;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author nopil
 */
@Entity
@Table(name = "Cajero")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Cajero.findAll", query = "SELECT c FROM Cajero c")
    , @NamedQuery(name = "Cajero.findByNickCajero", query = "SELECT c FROM Cajero c WHERE c.nickCajero = :nickCajero")
    , @NamedQuery(name = "Cajero.findByContrasena", query = "SELECT c FROM Cajero c WHERE c.contrasena = :contrasena")})
public class Cajero implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "NICK_CAJERO")
    private String nickCajero;
    @Basic(optional = false)
    @Column(name = "CONTRASENA")
    private String contrasena;

    public Cajero() {
    }

    public Cajero(String nickCajero) {
        this.nickCajero = nickCajero;
    }

    public Cajero(String nickCajero, String contrasena) {
        this.nickCajero = nickCajero;
        this.contrasena = contrasena;
    }

    public String getNickCajero() {
        return nickCajero;
    }

    public void setNickCajero(String nickCajero) {
        this.nickCajero = nickCajero;
    }

    public String getContrasena() {
        return contrasena;
    }

    public void setContrasena(String contrasena) {
        this.contrasena = contrasena;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (nickCajero != null ? nickCajero.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Cajero)) {
            return false;
        }
        Cajero other = (Cajero) object;
        if ((this.nickCajero == null && other.nickCajero != null) || (this.nickCajero != null && !this.nickCajero.equals(other.nickCajero))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "DTO.Cajero[ nickCajero=" + nickCajero + " ]";
    }
    
}
