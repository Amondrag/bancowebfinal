/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador;

import Negocio.Banco;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author nopil
 */
public class IngresoCajeroo extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
              try {
            String usuario = request.getParameter("usuario");
            String contrasena = request.getParameter("contra");
            Banco red = null;
            if (request.getSession().getAttribute("red") != null) {
                System.err.println("falso");
                    request.getSession().setAttribute("error", "Ya hay una sesion abierta, cierrela o siga en esta sesion");
                    request.getRequestDispatcher("./html/ingNoExitoso.html").forward(request, response);
            } else {
                red = new Banco();
                if (red.loguear(usuario, contrasena)){
                    request.getSession().setAttribute("red", red);
                    request.getSession().setAttribute("nick", usuario);                    
                    request.getRequestDispatcher("./jsp/Cajero/principal.jsp").forward(request, response);
                } else {
                    System.err.println("falso");
                    request.getSession().setAttribute("error", "Dato ya registrado en el sistema");
                    request.getRequestDispatcher("./html/ingNoExitoso.html").forward(request, response);

                }
            }

        } catch (Exception e) {

        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
