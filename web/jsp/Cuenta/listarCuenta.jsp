<%-- 
    Document   : listarCuenta
    Created on : 3/12/2019, 07:26:31 AM
    Author     : nopil
--%>

<%@page import="java.util.List"%>
<%@page import="DTO.*"%>
<%@page import="DAO.*"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="Negocio.Banco"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
         <%
        Banco banco=(Banco)(request.getSession().getAttribute("banco"));
        request.getSession().setAttribute("banco", banco);
        int client = (int) (request.getSession().getAttribute("cliente"));
        Cliente clien = banco.buscarCliente(client);
        List<Cuenta> datos = clien.getCuentaList();
        %>
        <h1>A continuacion listo las cuentas del cliente</h1>
        <table>
            <tr> 
            <th>nroCuenta</th>
            <th>Saldo</th>
            <th>Tipo de cuenta</th>
            </tr>
            <%
               for(Cuenta dato : datos){       
            %>
               <tr>
                <td><%=dato.getNroCuenta()%></td> 
                <td><%=dato.getSaldo()%></td>
                <td><%=dato.getTipo().getNombre()%></td>
            <tr/>
            <%
                }
            %>
        </table>
        <br>
        <br>
        <a href="./redirect.jsp">volver</a>
    </body>
</html>
