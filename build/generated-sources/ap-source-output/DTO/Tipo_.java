package DTO;

import DTO.Cuenta;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2019-12-03T08:01:50")
@StaticMetamodel(Tipo.class)
public class Tipo_ { 

    public static volatile ListAttribute<Tipo, Cuenta> cuentaList;
    public static volatile SingularAttribute<Tipo, Integer> id;
    public static volatile SingularAttribute<Tipo, String> nombre;

}