package DTO;

import DTO.Cuenta;
import DTO.TipoMovimiento;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2019-12-03T08:01:50")
@StaticMetamodel(Movimiento.class)
public class Movimiento_ { 

    public static volatile SingularAttribute<Movimiento, Date> fecha;
    public static volatile SingularAttribute<Movimiento, Cuenta> nroCuenta;
    public static volatile SingularAttribute<Movimiento, TipoMovimiento> idTipoMovimiento;
    public static volatile SingularAttribute<Movimiento, Integer> valor;
    public static volatile SingularAttribute<Movimiento, Integer> id;

}