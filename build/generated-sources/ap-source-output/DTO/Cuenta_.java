package DTO;

import DTO.Cliente;
import DTO.Movimiento;
import DTO.Tipo;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2019-12-03T08:01:50")
@StaticMetamodel(Cuenta.class)
public class Cuenta_ { 

    public static volatile SingularAttribute<Cuenta, Tipo> tipo;
    public static volatile SingularAttribute<Cuenta, Date> fechacreacion;
    public static volatile SingularAttribute<Cuenta, Integer> nroCuenta;
    public static volatile SingularAttribute<Cuenta, Cliente> cedula;
    public static volatile ListAttribute<Cuenta, Movimiento> movimientoList;
    public static volatile SingularAttribute<Cuenta, Double> saldo;

}