package DTO;

import DTO.Movimiento;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2019-12-03T08:01:50")
@StaticMetamodel(TipoMovimiento.class)
public class TipoMovimiento_ { 

    public static volatile SingularAttribute<TipoMovimiento, String> descripcion;
    public static volatile ListAttribute<TipoMovimiento, Movimiento> movimientoList;
    public static volatile SingularAttribute<TipoMovimiento, Integer> id;

}