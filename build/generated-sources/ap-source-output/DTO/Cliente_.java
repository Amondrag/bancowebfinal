package DTO;

import DTO.Cuenta;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2019-12-03T08:01:50")
@StaticMetamodel(Cliente.class)
public class Cliente_ { 

    public static volatile SingularAttribute<Cliente, String> dircorrespondencia;
    public static volatile SingularAttribute<Cliente, Date> fechanacimiento;
    public static volatile SingularAttribute<Cliente, Integer> cedula;
    public static volatile ListAttribute<Cliente, Cuenta> cuentaList;
    public static volatile SingularAttribute<Cliente, Integer> telefono;
    public static volatile SingularAttribute<Cliente, String> nombre;
    public static volatile SingularAttribute<Cliente, String> email;

}