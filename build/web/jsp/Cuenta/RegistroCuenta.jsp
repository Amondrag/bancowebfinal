<%@page import="DTO.Tipo"%>
<%@page import="DAO.TipoJpaController"%>
<%@page import="DTO.Cliente"%>
<%@page import="java.util.List"%>
<%@page import="DAO.ClienteJpaController"%>
<%@page import="DAO.Conexion"%>
<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>Registro</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <link rel="stylesheet" href="../../css/main.css">
    </head>

    <body>
        <div class="container">
            <div class="row">
                <div class="col-sm-9 col-md-7 col-lg-5 mx-auto">
                    <div class="card card-signin my-5">
                        <div class="card-body">
                            <h5 class="card-title text-center">Registro Cuenta</h5>
                            <form class="form-signin" method="POST" action="../../crearCuenta.do"> 
                                <div class="">
                                    <input type="number" id="cuenta" class="form-control" placeholder="Numero De Cuenta" required name="cuenta">
                                    <br>
                                </div>
                                
                                <div class="">
                                    <label>Fecha Creacion</label>
                                    <input type="date" class="form-control" id="fecha" placeholder="Fecha" name="fecha">
                                    <br>
                                </div>
                                <div class="">
                                    <input type="number" id="cedula" class="form-control" placeholder="Cedula Cliente" required name="cedula">
                                    <br>
                                </div>
                                 <div class="">
                                    <label>Tipo (1->Ahorros   2->Corriente):</label>
                                  
                                    <select class="mdb-select md-form colorful-select dropdown-primary" name="res">
                                    <%
                                        Conexion con = Conexion.getConexion();
                                        try {
                                            String sql = "select * from responsable;";
                                            TipoJpaController clienteDao = new TipoJpaController(con.getBd());
                                            List<Tipo> cre = clienteDao.findTipoEntities();
                                            for (Tipo dato : cre) {                                                    
                                                out.println("<option>" + dato.toString2() + "</option>");
                                                }
                                           
                                        } catch (Exception e) {
                                            out.print(e.toString());
                                        }


                                    %>
                                    <br>
                                    </select>
                                </div>
                                <button class="btn btn-lg btn-primary btn-block text-uppercase" type="submit" name="registrar">registrar</button>
                                <hr class="my-4">

                                <a class="btn btn-lg btn-google btn-block text-uppercase" href="../Cajero/principal.jsp">volver</a>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    </body>

</html>