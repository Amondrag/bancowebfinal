<!DOCTYPE html>
<html lang="en">

    <head>

        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>Cuenta Registro Exitoso</title>

        <!-- Custom fonts for this theme -->
        <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/css?family=Lato:400,700,400italic,700italic" rel="stylesheet" type="text/css">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
        <!-- Theme CSS -->
        <link href="../../css/freelancer.css" rel="stylesheet">
        <link href="../../css/main.css" rel="stylesheet">

    </head>

    <body id="page-top">

        <!-- Navigation -->
        <nav class="navbar navbar-expand-lg bg-secondary text-uppercase fixed-top" id="mainNav">
            <div class="container">
                <img src="../../img/logo.png" width="30%">
                <a class="navbar-brand js-scroll-trigger"></a>

                <button class="navbar-toggler navbar-toggler-right text-uppercase font-weight-bold bg-primary text-white rounded" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                    Menu
                    <i class="fas fa-bars"></i>
                </button>
                <div class="collapse navbar-collapse" id="navbarResponsive">
                </div>
            </div>
        </nav>

        <!-- Masthead -->
        <header class="masthead bg-rojo text-white text-center">
            <div class="container d-flex align-items-center flex-column">

                <!-- Masthead Avatar Image -->
                <img class="masthead-avatar mb-5" src="../../img/logo.png" alt="">

                <!-- Masthead Heading -->
                <h1 class="masthead-heading text-uppercase mb-0">Operacion Exitosa</h1>
                <div class="container">

                    <div class="col-md-6 o">
                        <ul>
                            <br>
                            <a class="btn btn-lg btn-google btn-block text-uppercase" href="../Cajero/principal.jsp">volver</a>
                    </div>  


                </div>
            </div>
        </header>       
        <!-- Footer -->
        <footer class="footer text-center">
            <div class="container">
                <div class="row">

                    <!-- Footer Location -->
                    <div class="col-lg-4 mb-5 mb-lg-0">
                        <h4 class="text-uppercase mb-4"></h4>
                        <p class="lead mb-0"> 
                            <br> #0- a Avenida Gran Colombia No. 12E-96</p>
                    </div>

                    <!-- Footer Social Icons -->
                    <div class="col-lg-4 mb-5 mb-lg-0">
                        <h4 class="text-uppercase mb-4">Nuestras redes</h4>
                        <a class="btn btn-outline-light btn-social mx-1" href="https://www.facebook.com/Ufps.edu.co/">
                            <i class="fab fa-fw fa-facebook-f"></i>
                        </a>
                        <a class="btn btn-outline-light btn-social mx-1" href="https://twitter.com/UFPSCUCUTA">
                            <i class="fab fa-fw fa-twitter"></i>
                        </a>
                        <a class="btn btn-outline-light btn-social mx-1" href="https://ww2.ufps.edu.co/">
                            <i class="fab fa-fw fa-dribbble"></i>
                        </a>
                    </div>

                    <!-- Footer About Text -->
                    <div class="col-lg-4">
                        <h4 class="text-uppercase mb-4">ACERCA DE NOSOTROS</h4>
                        <p class="lead mb-0">Nuestro trabajo
                            <a href="https://scontent.fbog3-1.fna.fbcdn.net/v/t1.0-9/61536060_2513835175307401_1723404057571229696_n.jpg?_nc_cat=106&_nc_ohc=j15NcmFatEQAQmFvxn2YYYsb9rAJEr42sFH9-9wKEK8JPYyYPvX47Br8g&_nc_ht=scontent.fbog3-1.fna&oh=63a2feebd5a167ec48b7ac633c8ecbf0&oe=5E7C6A14">Conocenos (Mondra tomo la foto)</a>.</p>
                    </div>

                </div>
            </div>
        </footer>

        <!-- Copyright Section -->
        <section class="copyright py-4 text-center text-white2">
            <div class="container">
                <small>Copyright &copy; Banco Nezuko  2019</small>
            </div>
        </section>

        <!-- Scroll to Top Button (Only visible on small and extra-small screen sizes) -->
        <div class="scroll-to-top d-lg-none position-fixed ">
            <a class="js-scroll-trigger d-block text-center text-white rounded" href="#page-top">
                <i class="fa fa-chevron-up"></i>
            </a>
        </div>


        <!-- Bootstrap core JavaScript -->
        <script src="vendor/jquery/jquery.min.js"></script>
        <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

        <!-- Plugin JavaScript -->
        <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

        <!-- Contact Form JavaScript -->
        <script src="js/jqBootstrapValidation.js"></script>
        <script src="js/contact_me.js"></script>

        <!-- Custom scripts for this template -->
        <script src="js/freelancer.min.js"></script>

    </body>

</html>